﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace MuchMedia.TycoonTerrain
{

    [RequireComponent(typeof(TerrainController))]
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    [RequireComponent(typeof(MeshCollider))]
    public class TerrainRenderer : MonoBehaviour
    {
        TerrainController terrainController;
        MeshFilter meshFilter;
        MeshCollider meshCollider;

        List<Vector3> vertices;
        List<int> triangles;
        List<Vector2> uvs;

        Mesh mesh;

        void Start()
        {
            meshFilter = GetComponent<MeshFilter>();
            meshCollider = GetComponent<MeshCollider>();
            terrainController = GetComponent<TerrainController>();

            terrainController.OnHeightMapReady += RenderTerrain;
        }

        public void SaveMesh()
        {
            AssetDatabase.CreateAsset(mesh,"Assets/mesh.asset");
            AssetDatabase.SaveAssets();
        }

        void OnDestroy()
        {
            terrainController.OnHeightMapReady -= RenderTerrain;
        }

        public void RenderTerrain()
        {
            vertices = new List<Vector3>();
            triangles = new List<int>();
            uvs = new List<Vector2>();

            for (int x = 0; x < terrainController.mapSize - 1; x++)
            {
                for (int z = 0; z < terrainController.mapSize - 1; z++)
                {
                    GenerateQuad(x, z);
                }
            }

            mesh = new Mesh()
            {
                vertices = vertices.ToArray(),
                triangles = triangles.ToArray(),
                uv = uvs.ToArray()
            };

            Debug.Log("Created mesh with " + vertices.Count + " vertices (mapsize - 1 * mapsize - 1 * 4 = "  + ((terrainController.mapSize - 1) * (terrainController.mapSize - 1) * 4) + ")" );

            meshFilter.mesh = mesh;
            meshCollider.sharedMesh = mesh;
        }

        void GenerateQuad(int x, int z)
        {
            int verticesOffset = vertices.Count;

            vertices.Add(new Vector3(x + 0, terrainController.heightMap[x + 0, z + 0], z + 0));
            vertices.Add(new Vector3(x + 0, terrainController.heightMap[x + 0, z + 1], z + 1));
            vertices.Add(new Vector3(x + 1, terrainController.heightMap[x + 1, z + 1], z + 1));
            vertices.Add(new Vector3(x + 1, terrainController.heightMap[x + 1, z + 0], z + 0));

            uvs.AddRange(TerrainHelper.getUVs(terrainController.terrainMap[x + 0, z + 0]));

            /*uvs.Add(new Vector2(0, 0));
            uvs.Add(new Vector2(0, 1));
            uvs.Add(new Vector2(1, 1));
            uvs.Add(new Vector2(1, 0));*/

            triangles.AddRange(new int[] {
                verticesOffset + 0,
                verticesOffset + 1,
                verticesOffset + 3
            });

            triangles.AddRange(new int[] {
                verticesOffset + 1,
                verticesOffset + 2,
                verticesOffset + 3
            });
        }
    }
}