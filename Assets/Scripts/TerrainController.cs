﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MuchMedia.TycoonTerrain
{ 
    public class TerrainController : MonoBehaviour
    {
        public delegate void OnHeightMapReadyHandler();
        public event OnHeightMapReadyHandler OnHeightMapReady;                        

        [Tooltip("Number of vertices in x and z. (Number of quads in x and z is -1)")]
        [Range(2, 128)]
        public int mapSize = 10; //This is the number of X and Z heightpoints, the number of squares for X or Z is this number -1
        public int[,] heightMap; //Y value of vertices
        public TerrainHelper.TerrainTypes[,] terrainMap;

        public float seed;
        public int chunkX, chunkY;               

	    void Start ()
        {
            GenerateHeightMap();
	    }	    

        public void GenerateHeightMap()
        {
            heightMap = new int[mapSize, mapSize];
            terrainMap = new TerrainHelper.TerrainTypes[mapSize, mapSize];

            float perinValue;
            int roundedPerlinValue;
            for (int x = 0; x < mapSize; x++)
            {
                for (int z = 0; z < mapSize; z++)
                {
                    float xSample = (seed + ((chunkX * (mapSize - 1)) + x) * 0.05f) + 0.001f;
                    float zSample = (seed + ((chunkY * (mapSize - 1)) + z) * 0.05f) + 0.001f;

                    perinValue = Mathf.PerlinNoise(xSample, zSample) * 10;

                    /*
                    DEBUG Used for testing if chunk-seams match
                    if(
                        x == 0 && z == 0 || 
                        x == mapSize - 1 && z == mapSize - 1
                    )
                        Debug.Log("Chunk "+ chunkX + "," +chunkY + " Point " + x + "," + z + " sampled as "+ xSample + "," + zSample);*/
                    
                    //Debug.Log(perinValue);

                    roundedPerlinValue = Mathf.RoundToInt(perinValue);

                    heightMap[x, z] = roundedPerlinValue;
                }
            }

            for (int x = 0; x < mapSize - 1; x++)
            {
                for (int z = 0; z < mapSize - 1; z++)
                {
                    int highest = 0;

                    if (heightMap[x + 0, z + 0] >= highest)
                    {
                        highest = heightMap[x + 0, z + 0];
                    }

                    if (heightMap[x + 0, z + 1] >= highest)
                    {
                        highest = heightMap[x + 0, z + 1];
                    }

                    if (heightMap[x + 1, z + 1] >= highest)
                    {
                        highest = heightMap[x + 1, z + 1];
                    }

                    if (heightMap[x + 1, z + 0] >= highest)
                    {
                        highest = heightMap[x + 1, z + 0];
                    }

                    if (highest >= 8)
                        terrainMap[x, z] = TerrainHelper.TerrainTypes.Stone;
                    else if (highest >= 6)
                        terrainMap[x, z] = TerrainHelper.TerrainTypes.Grass;
                    else
                        terrainMap[x, z] = TerrainHelper.TerrainTypes.Mud;
                }
            }

            Debug.Log ("Generated " + heightMap.Length + " heightpoints");

            if (OnHeightMapReady != null)
                OnHeightMapReady.Invoke();
        }
    }
}