﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainHelper : MonoBehaviour
{
    public enum TerrainTypes
    {
        Grass, Stone, Mud, Etc
    }

    public static Vector2[] getUVs(TerrainTypes terrainType)
    {        
        if (terrainType == TerrainTypes.Grass)
        {
            return getUV(0, 0);

            /*return new Vector2[]
            {
                new Vector2(0, 0),
                new Vector2(0, 0.23f),
                new Vector2(0.23f, 0.23f),
                new Vector2(0.23f, 0)
            };*/
        }
        if (terrainType == TerrainTypes.Mud)
        {
            //Debug.Log(getUV(3,3)[3]);

            return getUV(3, 3);

            /*return new Vector2[]
            {
                new Vector2(0.75f, 0.75f),
                new Vector2(0.75f, 1f),
                new Vector2(1f, 1f),
                new Vector2(1f, 0.75f)
            };*/
        }
        if (terrainType == TerrainTypes.Stone)
        {
            return getUV(0, 3);

            /*return new Vector2[]
            {
                new Vector2(0, 0.76f),
                new Vector2(0, 1f),
                new Vector2(0.23f, 1),
                new Vector2(0.23f, 0.76f)
            };*/
        }
        else
        {
            throw new System.Exception("Invalid terrain type!");
        }
    }
    
    private static Vector2[] getUV(float x, float y)
    {
        float size = 1f / 4f;

        return new Vector2[]
        {
                new Vector2(x * size, y * size),
                new Vector2(x * size, (y + 1) * size),
                new Vector2((x + 1) * size, (y + 1) * size),
                new Vector2((x + 1) * size, y * size)
        };
    }
}